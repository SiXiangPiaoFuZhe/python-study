import pygame

# 飞船
class Ship():

    def __init__(self, screen):
        self.screen = screen
        # 加载图像
        self.image = pygame.image.load('images/ship.bmp')
        self.rect = self.image.get_rect()
        self.screen_rect = screen.get_rect()
        # 将飞船放在屏幕底部中央
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom
        # self.center = float(self.rect.centerx)
        # self.moving_right = False
        # self.moving_left = False
        # self.moving_up = False
        # self.moving_down = False
        # self.speed_factor = 1

    def blitme (self):
        # 指定位置绘制飞船
        self.screen.blit(self.image, self.rect)