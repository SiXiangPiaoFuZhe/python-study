import sys
import pygame
from settings import Settings
from ship import Ship

def run_game():
    # 初始化游戏创建一个屏幕
    pygame.init()
    ai_settings = Settings()

    # 设置显示窗口
    screen = pygame.display.set_mode((ai_settings.screen_width,ai_settings.screen_height))

    # 创建一艘飞船
    ship = Ship(screen)
    # 窗口名称
    pygame.display.set_caption("外星人")
    # # 设置背景色
    # bg_color = (230,230,230)

    # 开始游戏的主循环
    while True:
        # 监视键盘和鼠标事件
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        # 重新绘制
        screen.fill(ai_settings.bg_color)
        ship.blitme()
        # 重置，擦除旧屏幕
        pygame.display.flip()

run_game()