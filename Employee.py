class Employee:
	# 类变量
	empCount = 0

	def __init__(self,name):
		# 使用实例变量
		self.name = name
		# 使用类变量
		Employee.empCount += 1

	def getCount(self):
		print('empCount ',Employee.empCount)

	def getName(self):
		print('name ',self.name)

emp1 =  Employee('zs')
emp2 =  Employee('ls')
emp1.getCount()
emp1.getName()
emp2.getCount()
emp2.getName()