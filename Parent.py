class Parent:
	def __init__(self):
		print('父类的构造函数')

class Child(Parent):
	def __init__(self):
		super(Child,self).__init__()
		print('子类的构造函数')

child = Child()
