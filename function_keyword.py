def function_keyword(a,b=1,c=5):
	print('a = ',a,',b = ',b,',c = ',c)

function_keyword(100)
function_keyword(10,c=2)
function_keyword(b=8,c=0,a=8)
function_keyword(5,c=0,a=8)